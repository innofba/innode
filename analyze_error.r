# f - function y'=f(x, y); y(x0)=y0; h - step size; n - number of steps
euler <- function(f, x0, y0, h, n){
  # set init values
  x <- x0 
  y <- y0

  for(i in 1:n) {
      # compute next step with Euler method
      y0 <- y0 + h * f(x0, y0)
      x0 <- x0 + h
      # put computed points to corresponding vectors
      x <- c(x, x0)
      y <- c(y, y0)
  }

  return(data.frame(x = x, y = y))
}

#f - function y'=f(x, y); y(x0)=y0; h - step size; n - number of steps
eulerplus <- function(f, x0, y0, h, n) {
  # set init values
  x <- x0
  y <- y0

  for(i in 1:n) {
    # compute next step with improved Euler method
    m1 <- f(x0, y0)
    m2 <- f(x0 + h, y0 + h*m1)
    y0 <- y0 + h*(m1+m2)/2
    x0 <- x0 + h
    # put computed points to corresponding vectors
    x <- c(x, x0)
    y <- c(y, y0)
  }

  return(data.frame(x = x, y = y))
}

#f - function y'=f(x, y); y(x0)=y0; h - step size; n - number of steps
rungekutta <- function(f, x0, y0, h, n) {
  # set init values
  x <- x0
  y <- y0

  for(i in 1:n) {
    # compute next step with Runge-Kutta method
    k1 <- f(x0, y0)
    k2 <- f(x0 + h/2, y0 + h*k1/2)
    k3 <- f(x0 + h/2, y0 + h*k2/2)
    k4 <- f(x0 + h, y0 + h*k3)
    y0 <- y0 + h*(k1+2*k2+2*k3+k4)/6
    x0 <- x0 + h
    # put computed points to corresponding vectors
    x <- c(x, x0)
    y <- c(y, y0)
  }

  return(data.frame(x = x, y = y))
}

sol <- function(x) -x*(x^3-4)/(x^3+2) # excact solution
fun <- function(x, y) y^2/x^2-2 # y'=f(x, y)
x0 <- as.numeric(dlg_input("Enter x0", "")$res)
y0 <- as.numeric(dlg_input("Enter y0", "")$res)
X <- as.numeric(dlg_input("Enter X", "")$res)
steps_start <- as.numeric(dlg_input("Enter steps_start", "")$res)
steps_end <- as.numeric(dlg_input("Enter steps_end", "")$res)

mErrors <- cbind(0, 0, 0)

for(steps in steps_start:steps_end) {
  h <- (X-x0) / steps # compute step size
  
  euler_sol <- euler(fun, x0, y0, h, steps)
  eulerplus_sol <- eulerplus(fun, x0, y0, h, steps)
  rungekutta_sol <- rungekutta(fun, x0, y0, h, steps)
  
  real_sol <- data.frame(x = seq(x0, X, by=h), y = sol(seq(x0, X, by=h)))
  
  # construct solution matrix
  solution <- euler_sol["y"]
  solution <- cbind(solution, eulerplus_sol["y"])
  solution <- cbind(solution, rungekutta_sol["y"])
  solution <- cbind(solution, real_sol["y"])
  
  # construct error matrix
  errors <- abs(real_sol["y"] - euler_sol["y"])
  errors <- cbind(errors, abs(real_sol["y"] - eulerplus_sol["y"]))
  errors <- cbind(errors, abs(real_sol["y"] - rungekutta_sol["y"]))
  
  names(errors) <- c("euler", "eulerplus", "rungekutta")
  
  mErrors <- rbind(mErrors, colMeans(errors))
}

# draw plot
matplot(steps_start:steps_end, mErrors[-1,], type = c("p"),pch=1, col = c("red", "blue", "green", "black"))
